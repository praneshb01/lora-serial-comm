﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;

//by pranesh for serial communiation with Lora module

namespace SerialComm
{
    public partial class MainForm : Form
    {
        SerialPort ComPort = new SerialPort();
        
        internal delegate void SerialDataReceivedEventHandlerDelegate(object sender, SerialDataReceivedEventArgs e);
        internal delegate void SerialPinChangedEventHandlerDelegate(object sender, SerialPinChangedEventArgs e);
        private SerialPinChangedEventHandler SerialPinChangedEventHandler1;
        delegate void SetTextCallback(string text);
        string InputData = String.Empty;

        public MainForm()
        {
            InitializeComponent();
            SerialPinChangedEventHandler1 = new SerialPinChangedEventHandler(PinChanged);
            ComPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(port_DataReceived_1);
        }
     
        private void btnGetSerialPorts_Click(object sender, EventArgs e)
        {
            ScanPorts();
        }

        private void ScanPorts()
        {
            string[] ArrayComPortsNames = null;
            int index;
         //   string ComPortName = null;

            //Com Ports
            ArrayComPortsNames = SerialPort.GetPortNames();
            cboPorts.Items.Clear();

            for (index = 0; index < ArrayComPortsNames.Length; index++)
            {
                cboPorts.Items.Add(ArrayComPortsNames[index]);
            }

            if (ArrayComPortsNames.Length > 0)
            {
                cboPorts.Text = ArrayComPortsNames[0];
            }
            else
            {
                cboPorts.Text = "";
            }
        }



        private void port_DataReceived_1(object sender, SerialDataReceivedEventArgs e)
        {
            InputData = ComPort.ReadExisting();
            if (InputData != String.Empty)
            {
                this.BeginInvoke(new SetTextCallback(SetText), new object[] { InputData });
            }
        }
        private void SetText(string text)
        {
            this.rtbIncoming.Text += text;
            rtbIncoming.SelectionStart = rtbIncoming.Text.Length;
            rtbIncoming.ScrollToCaret();
        }
        internal void PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            SerialPinChange SerialPinChange1 = 0;
            bool signalState = false;

            SerialPinChange1 = e.EventType;
            switch (SerialPinChange1)
            {
                case SerialPinChange.Break:
                    //MessageBox.Show("Break is Set");
                    break;
                case SerialPinChange.CDChanged:
                    signalState = ComPort.CtsHolding;
                  //  MessageBox.Show("CD = " + signalState.ToString());
                    break;
                case SerialPinChange.CtsChanged:
                    signalState = ComPort.CDHolding;
                    //MessageBox.Show("CTS = " + signalState.ToString());
                    break;
                case SerialPinChange.DsrChanged:
                    signalState = ComPort.DsrHolding;
                    // MessageBox.Show("DSR = " + signalState.ToString());
                    break;
                case SerialPinChange.Ring:
                    //MessageBox.Show("Ring Detected");
                    break;
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            //SerialPinChangedEventHandler1 = new SerialPinChangedEventHandler(PinChanged);
            //ComPort.PinChanged += SerialPinChangedEventHandler1;
            //ComPort.Open();

            //ComPort.RtsEnable = true;
            //ComPort.DtrEnable = true;
            //btnTest.Enabled = false;

        }

        private void btnPortState_Click(object sender, EventArgs e)
        {
          
            if (btnPortState.Text == "Closed")
            {
                ComPort.PortName = Convert.ToString(cboPorts.Text);
                ComPort.BaudRate = Convert.ToInt32(cboBaudRate.Text);
                ComPort.DataBits = Convert.ToInt16(cboDataBits.Text);
                ComPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cboStopBits.Text);
                ComPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), cboHandShaking.Text);
                ComPort.Parity = (Parity)Enum.Parse(typeof(Parity), cboParity.Text);
                try {
                    ComPort.Open();
                    rtbOutgoing.Enabled = true;
                    btnHyperTerm.Enabled = true;
                    btnPortState.Text = "Open";
                }
                catch (Exception ex)
                {
                    rtbOutgoing.Enabled = false;
                    btnHyperTerm.Enabled = false;
                    btnPortState.Text = "Closed";
                    MessageBox.Show(ex.Message.ToString());
                }
               

            }

            else if (btnPortState.Text == "Open")
            {
                rtbOutgoing.Enabled = false;
                btnHyperTerm.Enabled = false;
                btnPortState.Text = "Closed";
                ComPort.Close();
            }
        }
        private void rtbOutgoing_KeyPress(object sender, KeyPressEventArgs e)
        {
            string toSend;
            if (e.KeyChar == (char)13) // enter key  
            {
                toSend = rtbOutgoing.Text.Trim(); //remove white space
                //need to send "\r\n" to get reply
                toSend = toSend + ("\r\n");
                ComPort.Write(toSend);
                //ComPort.Write("AT");
               // ComPort.Write("\r\n");
                rtbOutgoing.Text = "";
            }
            /*else if (e.KeyChar < 32 || e.KeyChar > 126)
            {
                e.Handled = true; // ignores anything else outside printable ASCII range  
            }
            else
            {
                //ComPort.Write(e.KeyChar.ToString());
                ComPort.Write(rtbOutgoing.Text);
                ComPort.Write("\r\n");
            }*/
        }
        //private void btnHello_Click(object sender, EventArgs e)
        //{
        //    ComPort.Write("Hello World!");
        //}

        private void btnHyperTerm_Click(object sender, EventArgs e)
        {
            string Command1 = rtbOutgoing.Text;
            string CommandSent;
            int Length, j = 0;

            Length = Command1.Length;

            for (int i = 0; i < Length; i++)
            {
                CommandSent = Command1.Substring(j, 1);
                ComPort.Write(CommandSent);
                j++;
            }
            ComPort.Write("\r\n");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //get first item print in text
        //    cboPorts.Text = ArrayComPortsNames[0];
            //Baud Rate
            cboBaudRate.Items.Add(300);
            cboBaudRate.Items.Add(600);
            cboBaudRate.Items.Add(1200);
            cboBaudRate.Items.Add(2400);
            cboBaudRate.Items.Add(9600);
            cboBaudRate.Items.Add(14400);
            cboBaudRate.Items.Add(19200);
            cboBaudRate.Items.Add(38400);
            cboBaudRate.Items.Add(57600);
            cboBaudRate.Items.Add(115200);
            cboBaudRate.Items.ToString();
            //get first item print in text
            cboBaudRate.Text = cboBaudRate.Items[9].ToString();
            //Data Bits
            cboDataBits.Items.Add(7);
            cboDataBits.Items.Add(8);
            //get the first item print it in the text 
            cboDataBits.Text = cboDataBits.Items[1].ToString();

            //Stop Bits
            cboStopBits.Items.Add("One");
            cboStopBits.Items.Add("OnePointFive");
            cboStopBits.Items.Add("Two");
            //get the first item print in the text
            cboStopBits.Text = cboStopBits.Items[0].ToString();
            //Parity 
            cboParity.Items.Add("None");
            cboParity.Items.Add("Even");
            cboParity.Items.Add("Mark");
            cboParity.Items.Add("Odd");
            cboParity.Items.Add("Space");
            //get the first item print in the text
            cboParity.Text = cboParity.Items[0].ToString();
            //Handshake
            cboHandShaking.Items.Add("None");
            cboHandShaking.Items.Add("XOnXOff");
            cboHandShaking.Items.Add("RequestToSend");
            cboHandShaking.Items.Add("RequestToSendXOnXOff");
            //get the first item print it in the text 
            cboHandShaking.Text = cboHandShaking.Items[0].ToString();

            ScanPorts();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            rtbIncoming.Text = "";
        }
    }
}
